﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace SimpleInject.Tests
{
    public class Benchmarks : MonoBehaviour
    {
        private const int AverageCount = 1000;
        private const int Iterations = 10000;
        private Action[] methods;
        private List<double>[] results = new List<double>[3];
        private List<int> indexes = new List<int>() {0, 1, 2};
        private TestInjectable target;

        private void OnEnable()
        {
            target = gameObject.AddComponent<TestInjectable>();
            gameObject.AddComponent<TargetComponent>();

            var warmupGeneric = BenchmarkGenericGetComponent(this);
            var warmupType = BenchmarkTypeGetComponent(this);
            var warmupInject = BenchmarkInjectGetComponent(target);

            for (var i = 0; i < results.Length; ++i)
            {
                results[i] = new List<double>(AverageCount);
            }

            methods = new Action[]
            {
                () =>
                {
                    UnityEngine.Profiling.Profiler.BeginSample("GetComponent<TargetComponent>");
                    results[0].Add(BenchmarkGenericGetComponent(this));
                    UnityEngine.Profiling.Profiler.EndSample();
                },
                () =>
                {
                    UnityEngine.Profiling.Profiler.BeginSample("GetComponent(type)");
                    results[1].Add(BenchmarkTypeGetComponent(this));
                    UnityEngine.Profiling.Profiler.EndSample();
                },
                () =>
                {
                    UnityEngine.Profiling.Profiler.BeginSample("Injector.Inject(target)");
                    results[2].Add(BenchmarkInjectGetComponent(target));
                    UnityEngine.Profiling.Profiler.EndSample();
                }
            };

            for (var i = 0; i < AverageCount; ++i)
            {
                RunTests();
            }

            UnityEngine.Debug.Log($"Averaging time over {AverageCount} runs with {Iterations} iterations.");
            UnityEngine.Debug.Log($"GetComponent<TargetComponent> Warmup:{warmupGeneric}ms Min:{Min(results[0])}ms Max:{Max(results[0])}ms Average: {Average(results[0])}ms");
            UnityEngine.Debug.Log($"GetComponent(type) Warmup:{warmupType}ms Min:{Min(results[1])}ms Max:{Max(results[1])}ms Average: {Average(results[1])}ms");
            UnityEngine.Debug.Log($"Injector.Inject(target) Warmup:{warmupInject}ms Min:{Min(results[2])}ms Max:{Max(results[2])}ms Average: {Average(results[2])}ms");
        }

        private void RunTests()
        {
            indexes = indexes.OrderBy(x => Random.value).ToList();

            for (var i = 0; i < indexes.Count; ++i)
            {
                methods[i]();
            }
        }

        private static double BenchmarkGenericGetComponent(Benchmarks monoBehaviour)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            for (var i = 0; i < Iterations; ++i)
            {
                monoBehaviour.target = monoBehaviour.GetComponent<TestInjectable>();
            }

            stopwatch.Stop();

            return stopwatch.Elapsed.TotalMilliseconds;
        }

        private static double BenchmarkTypeGetComponent(Benchmarks monoBehaviour)
        {
            var type = typeof(TestInjectable);
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            for (var i = 0; i < Iterations; ++i)
            {
                monoBehaviour.target = (TestInjectable)monoBehaviour.GetComponent(type);
            }

            stopwatch.Stop();

            return stopwatch.Elapsed.TotalMilliseconds;
        }

        private static double BenchmarkInjectGetComponent(TestInjectable target)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            for (var i = 0; i < Iterations; ++i)
            {
                Injector.Inject(target);
            }

            stopwatch.Stop();

            return stopwatch.Elapsed.TotalMilliseconds;
        }

        private static double Min(List<double> list)
        {
            var min = double.MaxValue;

            for (var i = 0; i < list.Count; ++i)
            {
                min = Math.Min(min, list[i]);
            }

            return min;
        }

        private static double Max(List<double> list)
        {
            var max = double.MinValue;

            for (var i = 0; i < list.Count; ++i)
            {
                max = Math.Max(max, list[i]);
            }

            return max;
        }

        private static double Average(List<double> list)
        {
            var average = 0.0;

            for (var i = 0; i < list.Count; ++i)
            {
                average += list[i];
            }

            return average / list.Count;
        }
    }
}