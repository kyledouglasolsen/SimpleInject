﻿using UnityEngine;

namespace SimpleInject.Tests
{
    public class TestInjectable : MonoBehaviour
    {
        [GetComponent] public TargetComponent target;

        private void Awake()
        {
            Injector.Inject(this);
        }
    }
}