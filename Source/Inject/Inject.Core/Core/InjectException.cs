﻿using System;

public class InjectException : Exception
{
    public InjectException()
    {
    }

    public InjectException(string message) : base(message)
    {
    }
}