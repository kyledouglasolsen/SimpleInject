﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace SimpleInject
{
    public static class ReflectCache
    {
        internal static readonly BindingFlags Flags = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public;
        internal static readonly Func<Type, bool> InjectablePredicate = type => type == typeof(IInjectableAttribute);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static InjectField<T>[] Get<T>()
        {
            return InjectFieldCache<T>.Fields;
        }
    }

    internal static class InjectFieldCache<T>
    {
        internal static readonly InjectField<T>[] Fields;

        static InjectFieldCache()
        {
            var type = typeof(T);
            var list = new List<InjectField<T>>();

            while (type != null)
            {
                foreach (var field in type.GetFields(ReflectCache.Flags))
                {
                    foreach (var attribute in field.GetCustomAttributes(false))
                    {
                        var attributeType = attribute.GetType();

                        if (attributeType.GetInterfaces().Count(ReflectCache.InjectablePredicate) <= 0)
                        {
                            continue;
                        }

                        list.Add(new InjectField<T>((IInjectableAttribute)attribute, field));
                    }
                }

                type = type.BaseType;
            }

            Fields = list.ToArray();
        }
    }
}