﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace SimpleInject
{
    public class InjectField<T>
    {
        private static ParameterExpression FieldParameter = Expression.Parameter(typeof(object));
        private static readonly ParameterExpression OwnerParameter = Expression.Parameter(typeof(object));
        private static readonly UnaryExpression Convert = Expression.Convert(OwnerParameter, typeof(T));

        private readonly IInjectableAttribute attribute;
        private readonly Type fieldType;
        private readonly Type realFieldType;
        private readonly Action<object, object> set;
        private readonly IInjectableResolver resolver;

        public InjectField(IInjectableAttribute attribute, FieldInfo field)
        {
            this.attribute = attribute;
            fieldType = field.FieldType;
            realFieldType = fieldType.IsArray ? fieldType.GetElementType() : fieldType;
            resolver = InjectableResolver.Get(attribute.GetType());

            var expressionField = Expression.Field(Convert, field);
            var convertField = Expression.Convert(FieldParameter, fieldType);
            var assign = Expression.Assign(expressionField, convertField);
            var lambda = Expression.Lambda<Action<object, object>>(assign, OwnerParameter, FieldParameter);
            set = lambda.Compile();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Inject(T instance)
        {
            set(instance, resolver.Resolve(attribute, instance, realFieldType));
        }

        public override string ToString()
        {
            return $"InjectField<{typeof(T)}> Reflected Field {fieldType}";
        }
    }
}