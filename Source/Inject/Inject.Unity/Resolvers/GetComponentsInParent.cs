﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using JetBrains.Annotations;
using UnityEngine;

namespace SimpleInject
{
    [MeansImplicitUse(ImplicitUseKindFlags.Assign)]
    [AttributeUsage(AttributeTargets.Field)]
    public class GetComponentsInParent : Attribute, IInjectableAttribute
    {
    }

    internal class GetComponentsInParentResolver : Component, IInjectableResolver<GetComponentsInParent>
    {
        private static readonly List<Component> Cache = new List<Component>();

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public object Resolve<T>(GetComponentsInParent attribute, T instance, Type type)
        {
            CustomGetComponentsInParent((instance as Component), type, Cache);

            var casted = Array.CreateInstance(type, Cache.Count);

            for (var i = 0; i < Cache.Count; ++i)
            {
                casted.SetValue(Cache[i], i);
            }

            return casted;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        object IInjectableResolver.Resolve<T>(IInjectableAttribute attribute, T instance, Type type)
        {
            return Resolve((GetComponentsInParent)attribute, instance, type);
        }

        private static void CustomGetComponentsInParent(Component component, Type type, List<Component> components)
        {
            component.GetComponents(type, components);

            var parent = component.transform.parent;

            if (parent != null)
            {
                CustomGetComponentsInParent(parent, type, components);
            }
        }
    }
}