﻿using System;
using System.Runtime.CompilerServices;
using JetBrains.Annotations;
using UnityEngine;

namespace SimpleInject
{
    [MeansImplicitUse(ImplicitUseKindFlags.Assign)]
    [AttributeUsage(AttributeTargets.Field)]
    public class GetComponentInParentExcludeSelf : Attribute, IInjectableAttribute
    {
    }

    internal class GetComponentInParentExcludeSelfResolver : IInjectableResolver<GetComponentInParentExcludeSelf>
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public object Resolve<T>(GetComponentInParentExcludeSelf attribute, T instance, Type type)
        {
            var component = (instance as Component);
            var parent = component.transform.parent;
            return parent != null ? parent.GetComponentInParent(type) : null;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        object IInjectableResolver.Resolve<T>(IInjectableAttribute attribute, T instance, Type type)
        {
            return Resolve((GetComponentInParentExcludeSelf)attribute, instance, type);
        }
    }
}