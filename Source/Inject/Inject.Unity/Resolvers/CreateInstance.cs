﻿using System;
using System.Runtime.CompilerServices;
using JetBrains.Annotations;
using UnityEngine;

namespace SimpleInject
{
    [MeansImplicitUse(ImplicitUseKindFlags.Assign)]
    [AttributeUsage(AttributeTargets.Field)]
    public class CreateInstance : Attribute, IInjectableAttribute
    {
    }

    internal class CreateInstanceResolver : Component, IInjectableResolver<CreateInstance>
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public object Resolve<T>(CreateInstance attribute, T instance, Type type)
        {
            return ScriptableObject.CreateInstance(type);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        object IInjectableResolver.Resolve<T>(IInjectableAttribute attribute, T instance, Type type)
        {
            return Resolve((CreateInstance)attribute, instance, type);
        }
    }
}