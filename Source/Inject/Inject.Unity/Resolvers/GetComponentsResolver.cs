﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using JetBrains.Annotations;
using UnityEngine;

namespace SimpleInject
{
    [MeansImplicitUse(ImplicitUseKindFlags.Assign)]
    [AttributeUsage(AttributeTargets.Field)]
    public class GetComponents : Attribute, IInjectableAttribute
    {
    }

    internal class GetComponentsResolver : Component, IInjectableResolver<GetComponents>
    {
        private static readonly List<Component> Cache = new List<Component>();

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public object Resolve<T>(GetComponents attribute, T instance, Type type)
        {
            (instance as Component).GetComponents(type, Cache);
            var casted = Array.CreateInstance(type, Cache.Count);

            for (var i = 0; i < Cache.Count; ++i)
            {
                casted.SetValue(Cache[i], i);
            }

            return casted;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        object IInjectableResolver.Resolve<T>(IInjectableAttribute attribute, T instance, Type type)
        {
            return Resolve((GetComponents)attribute, instance, type);
        }
    }
}