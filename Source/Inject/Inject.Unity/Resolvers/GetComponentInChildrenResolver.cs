﻿using System;
using System.Runtime.CompilerServices;
using JetBrains.Annotations;
using UnityEngine;

namespace SimpleInject
{
    [MeansImplicitUse(ImplicitUseKindFlags.Assign)]
    [AttributeUsage(AttributeTargets.Field)]
    public class GetComponentInChildren : Attribute, IInjectableAttribute
    {
    }

    internal class GetComponentInChildrenResolver : IInjectableResolver<GetComponentInChildren>
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public object Resolve<T>(GetComponentInChildren attribute, T instance, Type type)
        {
            return (instance as Component).GetComponentInChildren(type);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        object IInjectableResolver.Resolve<T>(IInjectableAttribute attribute, T instance, Type type)
        {
            return Resolve((GetComponentInChildren)attribute, instance, type);
        }
    }
}