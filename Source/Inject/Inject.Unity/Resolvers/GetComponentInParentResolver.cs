﻿using System;
using System.Runtime.CompilerServices;
using JetBrains.Annotations;
using UnityEngine;

namespace SimpleInject
{
    [MeansImplicitUse(ImplicitUseKindFlags.Assign)]
    [AttributeUsage(AttributeTargets.Field)]
    public class GetComponentInParent : Attribute, IInjectableAttribute
    {
    }

    internal class GetComponentInParentResolver : IInjectableResolver<GetComponentInParent>
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public object Resolve<T>(GetComponentInParent attribute, T instance, Type type)
        {
            return (instance as Component).GetComponentInParent(type);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        object IInjectableResolver.Resolve<T>(IInjectableAttribute attribute, T instance, Type type)
        {
            return Resolve((GetComponentInParent)attribute, instance, type);
        }
    }
}