﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using JetBrains.Annotations;
using UnityEngine;

namespace SimpleInject
{
    [MeansImplicitUse(ImplicitUseKindFlags.Assign)]
    [AttributeUsage(AttributeTargets.Field)]
    public class GetComponentsInChildren : Attribute, IInjectableAttribute
    {
    }

    internal class GetComponentsInChildrenResolver : Component, IInjectableResolver<GetComponentsInChildren>
    {
        private static readonly List<Component> Cache = new List<Component>();

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public object Resolve<T>(GetComponentsInChildren attribute, T instance, Type type)
        {
            CustomGetComponentsInChildren((instance as Component), type, Cache);

            var casted = Array.CreateInstance(type, Cache.Count);

            for (var i = 0; i < Cache.Count; ++i)
            {
                casted.SetValue(Cache[i], i);
            }

            return casted;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        object IInjectableResolver.Resolve<T>(IInjectableAttribute attribute, T instance, Type type)
        {
            return Resolve((GetComponentsInChildren)attribute, instance, type);
        }

        private static void CustomGetComponentsInChildren(Component component, Type type, List<Component> components)
        {
            component.GetComponents(type, components);

            var transform = component.transform;

            for (var i = 0; i < transform.childCount; ++i)
            {
                CustomGetComponentsInChildren(transform.GetChild(i), type, components);
            }
        }
    }
}