﻿using JetBrains.Annotations;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace SimpleInject
{
    [MeansImplicitUse(ImplicitUseKindFlags.Assign)]
    [AttributeUsage(AttributeTargets.Field)]
    public class CreateGameObject : Attribute, IInjectableAttribute
    {
        private const string DefaultName = "GameObject";
        private static readonly Type[] DefaultTypes = new Type[0];

        public readonly string Name;
        public readonly Type[] Types;

        public CreateGameObject() : this(DefaultName, DefaultTypes)
        {
        }

        public CreateGameObject(string name) : this(name, DefaultTypes)
        {
        }

        public CreateGameObject(Type[] types) : this(DefaultName, types)
        {
        }

        public CreateGameObject(string name, params Type[] types)
        {
            Name = name;
            Types = types ?? DefaultTypes;
        }
    }

    internal class CreateGameObjectResolver : Component, IInjectableResolver<CreateGameObject>
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public object Resolve<T>(CreateGameObject attribute, T instance, Type type)
        {
            return new GameObject(attribute.Name, attribute.Types);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        object IInjectableResolver.Resolve<T>(IInjectableAttribute attribute, T instance, Type type)
        {
            return Resolve((CreateGameObject)attribute, instance, type);
        }
    }
}